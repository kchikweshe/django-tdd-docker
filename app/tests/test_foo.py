import json
import os

from django.urls import reverse


def test_hello_world():
    assert "hello_world" == "hello_world"
    assert "foo" != "bar"


def test_ping(client):
    url = reverse("ping")
    response = client.get(url)
    content = json.loads(response.content)
    assert response.status_code == 200
    assert content["ping"] == "pong"


def test_environment_variables():
    allowed_hosts = os.environ.get("DJANGO_ALLOWED_HOSTS")
    assert allowed_hosts is not None
