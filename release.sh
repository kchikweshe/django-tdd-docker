#!/bin/sh

IMAGE_ID=$(docker inspect registry.heroku.com/fathomless-plains-28093/web --format={{.Id}})
PAYLOAD='{"updates": [{"type": "web", "docker_image": "'"$IMAGE_ID"'"}]}'

curl -n -v -X PATCH https://api.heroku.com/apps/fathomless-plains-28093/formation \
  -d "${PAYLOAD}" \
  -H "Content-Type: application/json" \
  -H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
  -H "Authorization: Bearer d61dd446-a0cb-41de-870a-a014b76ac3e9"
